/*global cordova, module*/

module.exports = {
    secured: function (name, successCallback, errorCallback) {
        cordova.exec(successCallback, errorCallback, "Lockdetect", "secured", [name]);
    }
};
